# TKInter Debug Challenge 3
# 12/14/15
# Brady Russ

from tkinter import *

class MilesGUI:
    def __init__(self):
        # Create the main window
        self.main_window = Tk()

        # Create four frames
        self.gallons_frame = Frame(self.main_window)
        self.miles_frame = Frame(self.main_window)
        self.mpg_frame = Frame(self.main_window)
        self.bottom_frame = Frame(self.main_window)

        # Create the widgets for the gallons frame
        self.gallons_label = Label \
                             (self.gallons_frame, \
                              text = 'Enter the number of gallons:')
        self.gallons_entry = Entry(self.gallons_frame, width = 10)


        # Create the widgets for the miles frame
        self.miles_label = Label \
                           (self.miles_frame, \
                            text = 'Enter the number of miles:')
        self.miles_entry = Entry(self.miles_frame, width = 10)                                 

        # Pack the miles frame widgets
        self.miles_label.pack(side ='left')
        self.miles_entry.pack(side = 'left')
        
        # Create the widgets for the mpg frame
        self.result_label = Label \
                            (self.mpg_frame, \
                             text = 'Miles Per Gallon = ')
        
        # Create a blank label 
        self.mpg = StringVar()
        self.mpg_label = Label(self.mpg_frame, \
                                       textvariable= self.mpg)
        # Pack the mpg frame widgets
        self.result_label.pack(side = 'left')
        self.mpg_label.pack(side = 'left')
                                                           
        # Create the two buttons in the bottom frame
        self.mpg_button = Button \
                          (self.bottom_frame, \
                           text = 'Calculate MPG', \
                           command = self.calculate_mpg)
        self.quit_button = Button \
                           (self.bottom_frame, \
                            text = 'Quit', \
                            command = self.main_window.destroy)
              
        # Pack the widgets in the bottom frame
        self.mpg_button.pack(side='left')
        self.quit_button.pack(side='left')
                
        # Pack the frames
        self.gallons_frame.pack()
        self.miles_frame.pack()
        self.mpg_frame.pack()
        self.bottom_frame.pack()

        # Enter the tkinter main loop
        mainloop()

    # Define the show_info function
    def calculate_mpg(self):
        # Get the values entered
        self.gallons = float(self.gallons_entry.get())
        self.miles = float(self.miles_entry.get())

        # Calculate mpg
        self.miles_per_gallon = float(self.miles) / self.gallons

        # Update the mpg_label
        self.mpg.set(self.miles_per_gallon)

# Create an instance of MilesGUI
mpg = MilesGUI()

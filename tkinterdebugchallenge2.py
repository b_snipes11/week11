# CIS177 Tkinter Quiz
# Programming Exercise 2

import tkinter

class LatinTranslatorGUI:
    def __init__(self):
        # Create the main window
        self.main_window = Tk()

        # Create two frames
        self.top_frame = Frame(self.main_window)
        self.bottom_frame = Frame(self.main_window)
        
        # Create a blank label in the top frame
        self.value = StringVar()
        self.word_label = Label(self.top_frame, \
                                        textvariable= self.value)
                                            
        # Create the buttons in the bottom frame
        self.sinister_button = Button(self.bottom_frame, \
                                              text = 'sinister', \
                                              command = self.show_word1)
        self.dexter_button = Button(self.bottom_frame, \
                                            text = 'dexter', \
                                            command = self.show_word2)
        self.medium_button = Button(self.bottom_frame,
                                            text = 'medium', \
                                            command = self.show_word3)                

        # Pack the label
        self.word_label.pack()
        
        # Pack the buttons
        self.sinister_button.pack(side = 'left')
        self.dexter_button.pack(side = 'left')
        self.medium_button.pack(side = 'left')

        # Pack the frames
        self.top_frame.pack()
        self.bottom_frame.pack()

        # Enter the tkinter main loop
        mainloop()

    # Define the show_word functions
    def show_word1(self):
        self.value.set('left')
    def show_word2(self):
        self.value.set('right')
    def show_word3(self):
        self.value.set('center')

# Create an instance of LatinTranslatorGUI
latin_translator = LatinTranslatorGUI()
